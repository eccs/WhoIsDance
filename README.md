# WhoIsDance
Interactive digital installation based on skeleton detection with Kinect v2

1. Imagination is an abstract being with physical consequences in the big and small scale.
2. A transformation happens when you control a body different than yours.
3. At the end, we are all movement creators.

*Who is dance?* Is an interactive digital installation based on skeleton detection with Kinect v2. A virtual character is assigned to the user that enters the installation. The virtual character  is generated from diverse components for the body sections and is altered in the proportions between extremities. This character accurately follows the user’s movements and respects the implications of the new corporal configuration: the real right arm will move the virtual right arm, although it might be smaller or bigger. We invite the users to explore the motor possibilities of their character, and to question to what extent they are a reflection or an expansion of themselves.

## Media
* [*Who is dance?* in escenaconsejo.org](http://escenaconsejo.org/trayectoria/medios-digitales-interactivos/who-is-dance/)
* [*Who is dance?* English video in Vimeo](https://vimeo.com/164518657)
* [*Who is dance?* Spanish video in Vimeo](https://vimeo.com/164364397)


## Requirements
* Windows 8, 8.1, 10
* [Processing 3.0](http://processing.org)
* A Kinect for Windows V2 device
* [KinectPV2 Processing library](https://github.com/ThomasLengeling/KinectPV2) by [Thomas Sánchez Lengeling](https://github.com/ThomasLengeling)
