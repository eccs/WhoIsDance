/*
    Who is dance?
    Interactive digital installation based on skeleton detection with Kinect v2

    Copyright (C) 2016 José Vega-Cebrián (Sejo) 
    jmvc[at]escenaconsejo.org - www.escenaconsejo.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import KinectPV2.*;

KinectPV2 kinect; // Kinect library object
KinectSkeletonDepth ksd; // Manager of the Kinect information

SkeletonManager skes; // Manager of the skeletons (points, indices, segment types...)

float ytranslate;
float scaleFactor;

void setup(){
	fullScreen(P3D);

	// Skeleton Manager
	skes = new SkeletonManager();

	// Kinect
	kinect = new KinectPV2(this);
	kinect.enableDepthImg(true);
	kinect.enableSkeletonDepthMap(true);
	kinect.init();
	ksd = new KinectSkeletonDepth();
	ksd.setup();


	// Scaling and y translation
	scaleFactor = width/kinect.WIDTHDepth;
	ytranslate = 0;

}

void draw(){
	background(0);

	pushMatrix();
	translate(0,ytranslate,0);
	scale(scaleFactor,scaleFactor);

	// Update data from kinect
	ksd.update();
	// Update the SkeletonManager with new data
	skes.updateSkeletonsWithLists(ksd.getPoints(),ksd.getColors());
	// Draw the Skeletons
	skes.draw();
	popMatrix();
  

  // Message to show if there are no skeletons detected
  if(skes.getNumberOfSkeletons()<1){
    fill(255);
    textSize(80);
    String title = "Who is dance?";
    text(title,width/2-textWidth(title)/2,height/2);
    
    if((frameCount/30) % 2 == 0){
     textSize(60);
     String entra = ">>ENTRA<<";
     text(entra,width/2-textWidth(entra)/2,height*2/3);
    }
  }


}

void saveFrameWithDate(){
	save(String.format("frame-%04d%02d%02d%02d%02d%02d%03d.png",year(),month(),day(),hour(),minute(),second(),frameCount%1000));
}

void keyPressed(){
	switch(key){
		// Y translation
		case 'j':
			ytranslate += 10;
		break;
		case 'k':
			ytranslate -= 10;
		break;
		case 'J':
			ytranslate += 100;
		break;
		case 'K':
			ytranslate -= 100;
		break;

		// Scale factor
		case 'n':
			scaleFactor -= 0.1;
		break;
		case 'm':
			scaleFactor += 0.1;
		break;
	}

}
